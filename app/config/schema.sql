DROP TABLE IF EXISTS USER;
DROP TABLE IF EXISTS STATUSES;

--
-- Structure de la table `Statuses`
--
CREATE TABLE USER(
  user_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  user_name VARCHAR(100) NOT NULL,
  user_password VARCHAR(100) NOT NULL
);

--
-- Structure de la table `Statuses`
--
CREATE TABLE STATUSES(
  status_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  status_message VARCHAR(500) NOT NULL,
  status_user_id INT,
  status_date DATE NOT NULL
);




  -- FOREIGN KEY (status_user_id) REFERENCES USER(user_id) ON DELETE CASCADE